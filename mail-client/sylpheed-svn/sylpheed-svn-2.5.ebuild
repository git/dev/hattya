# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit autotools eutils subversion

IUSE="crypt ipv6 ldap nls oniguruma pda spell ssl xface"

ESVN_REPO_URI="svn://sylpheed.sraoss.jp/sylpheed/trunk"
ESVN_BOOTSTRAP="./autogen.sh -V"
ESVN_PATCHES="${ESVN_PROJECT}-2.*.diff"

DESCRIPTION="A lightweight email client and newsreader"
HOMEPAGE="http://sylpheed.sraoss.jp/"
SRC_URI=""

LICENSE="GPL-2 LGPL-2.1"
KEYWORDS="~ia64 ~x86"
SLOT="0"

COMMON_DEPEND=">=x11-libs/gtk+-2.4
	nls? ( >=sys-devel/gettext-0.12.1 )
	crypt? ( >=app-crypt/gpgme-0.4.5 )
	ldap? ( >=net-nds/openldap-2.0.11 )
	oniguruma? ( dev-libs/oniguruma )
	pda? ( app-pda/jpilot )
	spell? ( app-text/gtkspell )
	ssl? ( dev-libs/openssl )"
DEPEND="${COMMON_DEPEND}
	dev-util/pkgconfig
	xface? ( >=media-libs/compface-1.4 )"
RDEPEND="${COMMON_DEPEND}
	app-misc/mime-types
	x11-misc/shared-mime-info
	!mail-client/sylpheed"

src_compile() {

	local htmldir=/usr/share/doc/${PF}/html

	econf \
		$(use_enable crypt gpgme) \
		$(use_enable ipv6) \
		$(use_enable ldap) \
		$(use_enable nls) \
		$(use_enable oniguruma) \
		$(use_enable pda jpilot) \
		$(use_enable spell gtkspell) \
		$(use_enable ssl) \
		$(use_enable xface compface) \
		--with-manualdir=${htmldir}/manual \
		--with-faqdir=${htmldir}/faq \
		|| die
	emake || die

}

src_install() {

	emake DESTDIR="${D}" install || die

	dodoc AUTHORS ChangeLog* NEWS* README* TODO*
	doicon *.png
	domenu *.desktop

}

# $Id$
